MongoDB version 4.2, localhost, sem autenticação, Banco de dados chamado: "bot".

-----------------

Exemplos de chamadas:

Para retornar todas as mensagens de uma conversa:
GET
http://localhost:3000/messages?conversationId=996678ea-54f2-4181-81f5-5207655961e0

-----------------

Para retornar todas as mensagens independente de conversas
GET
http://localhost:3000/messages

-----------------

Para retornar uma mensagem específica:
GET
http://localhost:3000/messages/5ea8392032772c488c7f0323

-----------------

Para enviar uma mensagem:
POST
http://localhost:3000/messages

Parametro primeira mensagem:
{
    "text": "quero frango batata e alface"
}

Ao enviar a primeira mensagem, junto com a resposta do bot, será enviado o "conversationId", utilize como parâmetro para continuar a mesma conversa.

Parametros demais mensagens da mesma conversa:
{
    "text": "quero frango batata e alface",
    "conversationId": "996678ea-54f2-4181-81f5-5207655961e0"
}