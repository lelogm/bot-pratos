const express = require('express');

const mongoose = require('mongoose');
require('dotenv').config();

const bodyParser = require('body-parser')

// App
const app = express();


// Database
mongoose.connect(process.env.DATABASE_CONNECTION_STRING, {
    useNewUrlParser: true
});

const db = mongoose.connection;
  
db.on('connected', () => {
    console.log('Mongoose default connection is open');
});

db.on('error', err => {
    console.log(`Mongoose default connection has occured \n${err}`);
});

db.on('disconnected', () => {
    console.log('Mongoose default connection is disconnected');
});

process.on('SIGINT', () => {
    db.close(() => {
        console.log(
        'Mongoose default connection is disconnected due to application termination'
        );
        process.exit(0);
    });
});




app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))



// Carrega model
const Messages = require('./model/messages');



// Carrega routes
const botRoutes = require('./routes/bots-routes');
app.use('/bots', botRoutes);

const messagesRoutes = require('./routes/messages-routes');
app.use('/messages', messagesRoutes);










module.exports = app;
