const express = require('express');
const router = express.Router();
const messagesController = require('../controller/messages-controller');

// Padroniza o carregamento de variaveis
router.param('id', messagesController.listMessageById);

// Direciona a chamada para a função que irá tratar e responder
router.get('/', messagesController.listMessages); //Direciona requisição GET para a função listaMessages
router.get('/:id', messagesController.listMessageById); //Direciona requisição GET para a função listaMessageById
router.post('/', messagesController.sendMessage); //Direciona requisição POST para a função cadastraMessage

module.exports = router;
