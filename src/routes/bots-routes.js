const express = require('express');
const router = express.Router();
const botController = require('../controller/bots-controller');

// Padroniza o carregamento de variaveis
router.param('id', botController.listaBots);

// Direciona a chamada para a função que irá tratar e responder
router.get('/', botController.listaBots); //Direciona requisição GET para a função listaBots
router.get('/:id', botController.listaBotById); //Direciona requisição GET para a função listaBotById
router.post('/', botController.cadastraBot); //Direciona requisição POST para a função cadastraBot

module.exports = router;
