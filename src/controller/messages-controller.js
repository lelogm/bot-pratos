const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');
const mongoose = require('mongoose');
const Messages = mongoose.model('Messages');

const assistant = new AssistantV2({
  version: '2020-03-23',
  authenticator: new IamAuthenticator({
    apikey: 'HAzzMQCybUpeO_l6LPH1SeFA0mg_QRzzcZ_KIDvUISDw',
  }),
  url: 'https://api.eu-gb.assistant.watson.cloud.ibm.com/instances/3bdf4f95-6f9d-40a1-a97d-21fa3d80754f',
});

const assistantId = 'da04b6f7-a83f-4271-a6b3-93024a62a3d9'

// lista mensagens
exports.listMessages = async (req, res) => {

  if(req.query.conversationId){
    var data = await Messages.find({'conversationId': req.query.conversationId})
  }else{
    var data = await Messages.find({})
  } 
  
  
  
  res.status(200).send(data);

};




// lista mensagem by id
exports.listMessageById = async (req, res, id) => {

  var data = await Messages.find({'_id': mongoose.Types.ObjectId(req.params.id)});

  res.status(200).send(data);
  
};




// envia mensagem
exports.sendMessage = async (req, res) => {
  
  if(req.body.conversationId){
    var sessionId = req.body.conversationId
  }else{

    var sessionId = await assistant.createSession({
      assistantId: assistantId
    })

    sessionId = sessionId.result.session_id
  }


  var message = await assistant.message({
    assistantId: assistantId,
    sessionId: sessionId,
    input: {
      'return_context': true,
      'message_type': 'text',
      'text': req.body.text
      }
    })

    const usrMessage = new Messages({
      conversationId: sessionId,
      timestamp: Date.now(),
      to: assistantId,
      from: "user",
      text: {message: req.body.text}
    });
    await usrMessage.save();

    const botMessage = new Messages({
      conversationId: sessionId,
      timestamp: Date.now(),
      to: "user",
      from: assistantId,
      text: message.result.output.generic
    });
    await botMessage.save();
    

    res.status(200).send({conversationId: sessionId, response: message.result.output.generic});
  
};