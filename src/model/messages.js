const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  conversationId: {
    type: String,
    required: true,
    trim: true
  },
  timestamp: {
    type: String
  },
  from: {
    type: String
  },
  to: {
    type: String
  },
  text: {
    type: Object
  }
});

module.exports = mongoose.model('Messages', schema);
